// Simpler Rechner der zwei Zahlen miteinander substrahieren,
// addieren, dividieren oder multiplizieren kann

package main

import "fmt"

func add(x float64, y float64) float64 {
	return x + y
}

func substract(x float64, y float64) float64 {
	return x - y
}

func multiply(x float64, y float64) float64 {
	return x * y
}

func divide(x float64, y float64) float64 {
	return x / y
}

func main() {
	var inputChoice int
	var inputN1 float64
	var inputN2 float64

	fmt.Println("Wähle eine Option aus:")
	fmt.Println("1. Addieren")
	fmt.Println("2. Subtrahieren")
	fmt.Println("3. Multiplizieren")
	fmt.Println("4. Dividieren")
	fmt.Println("---")

	// Eingaben des Benutzers bezüglich der Rechenart in einer Variablen speichern
getIntput:
	fmt.Scanln(&inputChoice)

	// Überprüfen der Benutzereingabe
	if !(inputChoice <= 4) {
		fmt.Printf("%v ist keine gültige Option!\nVersuche es nochmal: ", inputChoice)
		goto getIntput
	}

	// Eingabe des Benutzers bezüglich der zwei Zahlen in jeweils einer Variablen speichern
	fmt.Println("Gebe nun zwei durch ein Leerzeichen getrennte Zahlen ein:")
	fmt.Scanln(&inputN1, &inputN2)

	if inputChoice == 1 {
		fmt.Println(inputN1, "+", inputN2, "=", add(inputN1, inputN2))
	} else if inputChoice == 2 {
		fmt.Println(inputN1, "-", inputN2, "=", substract(inputN1, inputN2))
	} else if inputChoice == 3 {
		fmt.Println(inputN1, "*", inputN2, "=", multiply(inputN1, inputN2))
	} else if inputChoice == 4 {
		fmt.Println(inputN1, "/", inputN2, "=", divide(inputN1, inputN2))
	}
}
