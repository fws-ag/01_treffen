# Installation der benötigten Software auf Windows (7 oder >)
Wir benötigen die folgenden drei Programme, um unter Windows >= 7 mit Go zu arbeiten:

- Git For Windows
- Go
- Visual Studio Code

## Git For Windows
Um Git For Windows zu installieren, gehe auf https://gitforwindows.org/ und klicke auf den großen, blauen Download-Button. Führe anschließend die heruntergeladene Datei - Git 2.xx-64-bit.exe - aus, und folge den Anweisungen des Installers. Lediglich eine einzige Option sollte während der Installation verändert werden, und zwar der standardmäßig verwendete Text-Editor. Wähle im entsprechenden Schritt der Installation "Visual Studio Code" als Text-Editor aus.

**Starte dein System neu**.

## Visual Studio Code
Gehe auf https://code.visualstudio.com/, klicke auf "Download for Windows".  
Führe die heruntergeladene Datei aus und folge den Schritten des Installers.  

Öffne Visual Studio Code nachdem die Installation abgeschlossen ist, und klicke auf das quadratische Symbol in der Leiste, die sich auf der linken Seite des Programmes befindet **oder** drücke Strg+Shift+X.  
In der Suchleiste, die sich nun öffnet, gib "Go" ein.
Klicke auf den, wahrscheinlich obersten, Eintrag "Go" und dann auf den kleinen grünen Button "Install", anschließend auf den nun erschienenen Button "Reload".  
Gehe nun noch auf Settings (Zahnrad-Icon am unteren Ende der linken Leiste) und suche nach der Option "Format on Save". Aktiviere die Option, indem du auf das Kästchen klickst, so dass ein Häckchen darin zu sehen ist.  

Suche außerdem noch nach der Option "Terminal › Integrated › Shell: Windows", und gib "C:\Program Files\Git\bin\bash.exe" (Ohne ") in das Feld der Option ein.

## Go

### Installation
Gehe auf https://golang.org/ - Die außergewöhnlich hässlige Website der Programmiersprache :p - und klicke auf "Download Go". Klicke unter "Featured Downloads" auf "Microsoft Windows". Führe die heruntergeladene Datei aus.

Folge nun den Schritten des Installers, wähle als "Destination Folder" das standartmäßige Verzeichnis C:\Go\\.

### Konfiguration
Erstelle den folgenden Ordner: _C:\Users\DEIN_BENUTZERNAME\\_**go**, Bei mir z.B. C:\Users\linus\go.  

Öffne nun das vorher installierte Programm "Git Bash", ein Terminal.  
Gebe im Terminal "ls" ein und drücke Enter.  
Dieser Befehl listet alle Dateien und Ordner auf, die sich in dem momentanen Ordner befinden.  
Du solltest in der Liste den gerade von dir erstellten Ordner "go/" finden.  
Gib "cd go/" in das Terminal ein und drücke Enter. Du befindest nun im Ordner "go-work/".  
Gib danach "mkdir -p src/hello" ein und bestätige mit Enter.  
Öffne nun Visual Studio Code und wähle in der Menüzeile am oberen Rand des Programmes unter dem Reiter "File" die Option "Open Folder" aus. Öffne C:\Users\\*DEIN_BENUTZERNAME*\go.  

Auf der linken Seite sollte nun ein Panel mit dem Titel "Explorer" zu sehen sein. Wenn nicht, drücke auf das Datei-Icon in der linken Leiste oder drücke Strg+Shift+E.  
Erstelle im Ordner src/hello die Datei hello.go, per Rechtsklick im "Explorer" Panel.
Öffne die Datei.  
Gib folgendes in die Datei ein:

```go
package main

import "fmt"

func main() {
	fmt.Printf("hello, world\n")
}
```

Speichere mit Strg+s oder "File -> Save".

Wenn ein Dialog aufkommt, der nach einer Installation von Go-Tools fragt, klicke auf "Install All". Wichtig ist, dass du nach der Installation von Git dein System neu gestartet hast!

Drücke Strg+Shift+ö, dies öffnet ein Terminal innerhalb Visual Studio Codes.  

Im terminal Fenster sollte etwa stehen:

```
linus@DESKTOP-QL755PU MINGW64 ~/go
$
```

Gib nun "go build src/hello/hello.go" in das Terminal ein, und bestätige mit Enter. Im Ordner C:\Users\DEIN_BENUTZERNAME\go sollte nun eine neue Datei, hello.exe, existieren. Du solltest sie im Explorer Panel sehen können.  

Gib "./hello.exe" in das Terminal ein, bestätige mit Enter.
Im Terminal sollte nun der Text "hello, world" erscheinen!

Wenn dies geklappt hat, hast du erfolgreich alle Programme installiert und bist bereit für die Beispiele und Übungen. Falls es nicht geklappt hat, ließ dir die Anweisungen noch einmal gründlich durch und schreib' mir dann auf Telegram oder so :P

Als nächsten Schritt solltest du die Datei "variablen.go" im Ordner 01_variablen/src öffnen, und dir den Inhalt durchlesen.