// In der Vielzahl aller Fälle kommen die Daten, die ein Programm verarbeiten muss von "außen".
// Bisher waren alle Daten, mit denen wir gearbeitet haben schon vor der Ausführung festgelegt, was nicht
// besonders realistisch ist.
//
// Während des unendlich viele potenzielle Quellen für Daten gitb, mit denen ein Programm sich befassen
// kann, werden wir nun erst einmal auf das Terminal eingehen.
// Das Terminal verfügt über drei primäre Schnittstellen:
// stdin - Standard Input,
// stdout - Standard Output,
// stderr - Standard Error.
//
// Wir befassen uns jetzt mit der Aufgabe, Daten, die durch die Schnittstelle stdin verarbeitet werden,
// in unserem Programm zu benutzen.
// stdin ist die Schnittstelle, mit der du interagierst wenn du z.B. Befehle in das Terminal eingibst.

package main

import "fmt"

func main() {
	//  ---------- Beispiel ----------

	// Wir wollen einen Benutzer unseres großartigen Programms persönlich ansprechen,
	// hierfür möchten wir seinen Namen wissen.+
	// Wir bitten ihn, den Namen in das Terminal einzugeben und begrüßen ihn anschließend:

	// Wir Deklarieren eine "leere" Variable, der der Wert des Namens zugewiesen werden soll,
	// sobald wir ihn erfahren
	var name string

	fmt.Print("Bitte gib' deinen Namen ein: ")

	// die Funktion Scanln aus der fmt Bibliothek ließt und scannt Text aus stdin
	// und speichert aufeinanderfolgende, durch Leerzeichen getrennte Werte in aufeinanderfolgenden
	// Argumenten. Zeilenumbrüche gelten als Leerzeichen. Es wird die Anzahl der erfolgreich gescannten Elemente zurückgegeben.
	// Wenn das weniger als die Anzahl der Argumente ist, wird err* den Grund dafür melden.
	//
	// Ein weiterer unbekannter Aspekt ist das "&" Zeichen.
	// Es ist ein Pointer. Über Pointer werden wir später noch viel hören, für jetzt ist es wichtig
	// zu wissen, dass &name quasi auf den Wert name verweist - oder zeigt -.
	//
	// & vor dem Variablennamen wird verwendet, um die Adresse abzurufen,
	// an der der Wert dieser Variablen im Arbeitsspeicher gespeichert ist.
	// Diese Adresse ist es, was der Pointer speichern wird.
	fmt.Scan(&name)

	fmt.Printf("Hi %v! Danke, dass du mir deinen Namen verraten hast!", name)

	// !# Ergänze um error handling #!
}
