// In diesem Programm werden Variablen eingeführt.
// Denk dran, dass du das Programm mit "go run /Pfad/Zum/Quellcode ausführst"
// Öffne ein Terminal via Strg+Shift+ö
// gib ein "go run 01_variablen/src/variablen.go" und bestätige mit Enter
//
// Lies dir den Quellcode und die Kommentare durch, und experimentiere einfach ein wenig!
// Nach jeder Änderung kannst du das Programm dann per dem oben genannten Befehl ausführen, um die
// Effekte zu untersuchen

// Zu Beginn noch ein paar Begriffserläuterungen:
//
// 1. Deklaration - Bindet eine Kennung (Identifier) an eine Konstante, Variable, Funktion, Label oder Package.
// - Jede verwendete Kennung in einem Programm muss in dem Block deklariert worden sein, in dem sie referenziert wird.
// - Eine Kennung darf in einem Block nicht doppelt deklariert werden.
//
// 2. Scope und Block:
// - {...} ist ein Block. Der Inhalt des Blocks befindet sich innerhalb der geschweiften Klammern.
// - Scope, zu Deutsch in unserem Falle Anwendungs -oder Geltungsbereich, beschreibt den Bereich des
// Programms, in dem eine Kennung als deklariert gilt und referenziert werden darf.
//
// 3. Statement:
// - Ein Statement, zu Deutsch Anweisung, steuert die Ausführung eines Go Programms.
// - Ein Statement ist eine klar formulierte Anweisung, bestenfalls ohne Ambiguität.
// - Beispiel: fmt.Println("Print this!")
//
// - Zusammengefasst ist ein Statement:
// Eine syntaktische Einheit einer imperativen* Programmiersprache,
// die eine auszuführende Aktion ausdrückt.
//
// 4. Expression:
// - Eine Expression, zu Deutsch Ausdruck, gibt die Berechnung eines Werts an,
// indem Operatoren und Funktionen auf Operanden angewendet werden.
//
// Beispiel: 1 + 1 -> 2          | [OPERAND OPERATOR OPERAND -> WERT]
//			(1 + 1) == 2 -> true | [(OPERAND OPERATOR OPERAND) OPERATOR OPERAND -> Wert]
//
// Merke: Ein Ausdruck ist jede gültige Codeeinheit, die in einen Wert aufgelöst wird.
//
// Okaaay, erstmal genug Theory von dieser Sorte...
// Auf zu mehr Theorie anderer Art!

// Beginn des eigentlichen Programms mit Deklaration der Packet Kennung
package main

// Import Statement
import "fmt"

// "fmt" (format) ist ein Bibliothek (Library), die zur "Standard Library" Go's gehört. Bibliotheken
// sind Programme, die Funktionalität für andere Programme zur Verfügung stellen, ohne an sich ausführbar
// zu sein. Sie stellen eine Reihe an Funktionen zur Verfügung, im Falle von "fmt" z.B. Printf (Print-Format)
// oder Println (Print-Line).

// Deklaration der Funktion main.
// main() dient als "Einstiegspunkt" in unser Programm. Die main Funktion wird automatisch
// und als erste ausgeführt. Mehr über das, was während der Ausführung eines Programms geschieht und
// wie wir es steuern können lernen wir in 02_kontrollstrukturen.
func main() {

	//  -------------------------  DEKLARATION VON VARIABLEN  -------------------------

	// Es gibt drei Wege, eine Variable zu deklarieren

	//  ----------- 1. Weg ----------
	// Vollständig ausgeschrieben:
	// keyword identifier typ assignment_operator wert
	var varA int = 10
	// ^ Wie du sehen kannst, wenn du das Go Plugin für Visual Studio Code installiert hast,
	// Ist die Deklaration grün unterstrichen. Dies indikiert eine Warnung! Wenn du den Mauszeiger auf
	// dem grün unterstrichenen Teil des Codes platzierst solltest du eine Fehlmeldung sehen, die
	// uns mitteilt, dass wir den Typ "int" weglassen sollten, weil er von der rechten Seite inferiert wird.
	// Der Code wird allerdings auch so fehlerfrei kompilieren und laufen!
	fmt.Printf("varA hat den Wert %v und repräsentiert den Typ %T\n", varA, varA)

	//  ----------- 2. Weg ----------
	// Mit Keyword aber ohne Typ:
	// keyword identifier assignment_operator wert
	var varB = 11
	fmt.Printf("varB hat den Wert %v und repräsentiert den Typ %T\n", varB, varB)
	// Der Typ von varB, int, wurde durch den Wert, 10, inferiert.

	//  ----------- 3. Weg -----------
	// Kurzform
	// identifier short_assignment_operator wert
	varC := 10.5
	fmt.Printf("varC hat den Wert %v und repräsentiert den Typ %T\n", varC, varC)

	// Generell ist der 3. Weg, die Kurzform der Deklaration, am angenehmsten und schnellsten.
	// In späteren Beispielen werden wir über Situation sprechen, in denen der Typ einer Variablen
	// NICHT inferiert werden kann, womit die Kurzform ungültig wird.
	//  -------------------------------------  #  --------------------------------------

	//  ----------------------  EINIGE BEISPIELE ZUR DEKLARATION  ----------------------

	// Deklaration mit Kurzform.
	num := 15.9
	fmt.Printf("num repräsentiert den Typ %T\n", num)

	// Kurzform zur Deklaration von mehreren Variablen auf einmal
	var name1, name2, name3 string = "Yassin", "Josef", "Jakob"
	// Das Selbe wie:
	// var name1 string = "Yassin"
	// var name2 string = "Josef"
	// var name3 string = "Jakob"

	whoToGreet := "Linus"

	// Informationen Zu if-statements findest du in 02_kontrollstrukturen
	// Die oben deklarierten Variablen können nun ganz normal referenziert werden
	if whoToGreet == name1 {
		fmt.Println("Hey Yassin!")
	} else if whoToGreet == name2 {
		fmt.Println("Hey Josef")
	} else if whoToGreet == name3 {
		fmt.Println("Hey Jakob!")
	} else {
		fmt.Printf("Hey %v!\n", whoToGreet)
	}

	// Deklaration mehrerer Variablen mit short_assignment_operator (:=)
	num1, num2, num3 := 1, 1, 2
	fmt.Printf("num1 = %v, num2 = %v, num3 = %v\n", num1, num2, num3)

	//  ------------------------------  PRIMITIVE TYPEN  ------------------------------

	// Eine Variable kann in Go einen der folgenden primitiven Typen repräsentieren:
	// bool
	// string
	// int int8 int16 int32 int64 - ganze Zahlen einschließlich negativer Werte
	// uint uint8 uint16 uint32 uint64 uintptr - ganze Zahlen außschließlich negativer Werte
	// byte - alias für uint8
	// rune - alias für uint32, repräsentiert einen Unicode code point https://de.wikipedia.org/wiki/Codepoint
	// float32 float64 - Zahlen mit Nachkommastellen, einschließlich negativer Werte
	// complex64 complex128
	//
	// Außer primitiven Typen gibt es auch noch andere, die wir selber definieren können.
	// Mehr zu solchen Typen lernen wir zu einem späteren Zeitpunkt.

	//  -------- Standardwerte -------
	// Deklaration einer Variablen OHNE EXPLIZITEM WERT:
	// Wird eine Variable, die ohne Wert deklariert wurde aufgerufen,
	// wird der Standardwert des jeweiligen Typs als Wert eingesetzt.
	// Im Falle von int ist der Standardwert 0.
	//
	// Ändere den Typ der Variablen und führe das Programm aus,
	// um die Standardwerte anderer Typen zu ermitteln!
	var keinWertDeklariert int
	fmt.Printf("Der Wert der Variablen keinWertDeklariert ist %v, der Standardwert für den Typ %T\n", keinWertDeklariert, keinWertDeklariert)

	//  ------------ bool -----------

	// Beginnen wir mit dem bool type
	// Ein bool, oder boolean, ist ein Wert mit einem von zwei möglichen Zuständen
	var boolean bool = false
	fmt.Println(boolean == false) // gibt "true" aus, weil der Wert boolean identisch zu false ist

	//  ------------ int ------------

	// Ein int, integer oder ganze Zahl, kann  ganze, positive oder negative numerische Werte
	// repräsentieren. Der Wertebereich ist endlich, so z.B. bei int8 -128 bis 127.
	// Warum? Erinnern wir uns an das Binärsystem/Dualsystem. https://de.wikipedia.org/wiki/Zweierkomplement
	//
	// 		Beispiel Binärsystem 8 Bit
	// ---      Wertigkeit der Bits      ---
	// -128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 | Dezimal
	//    0    0    1    0   0   0   0   0 = 32
	//    1    1    1    0   0   1   1   0 = -26
	//    1    0    0    0   0   0   0   0 = -128
	//    1    0    0    0   0   0   1   0 = -126
	//    0    1    1    1   1   1   1   1 = 127
	//
	// Warum -128 und nicht einfach minus/nicht-minus?
	// Zum einen elimiert es die Situation, zwischen negativer und positiver Null differenzieren
	// zu müssen, zum anderen funktioniert ein erstes Bit mit der Wertigkeit -128 im Grunde
	// genauso wie ein reines Vorzeichenbit.

	var integer8Bit int8 = 127
	fmt.Printf("integer8Bit hat den Wert %v und den Typ %T\n", integer8Bit, integer8Bit)

	//  ------------ uint ------------

	// uint steht für "unsigned integer", also ein Integer der im Binärsystem ohne Vorzeichenbit
	// repräsentiert wird, d.h., ein uint kann keinen negativen Wert repräsentieren!
	// Der maximale Wert eines uint8 ist 255.
	//
	//		Beispiel Binärsystem 8 Bit
	// ---      Wertigkeit der Bits      ---
	//  128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 | Dezimal
	//    0    0    1    0   0   0   0   0 = 32
	//    1    1    1    0   0   1   1   0 = 230
	//    1    0    0    0   0   0   0   0 = 128
	//    1    0    0    0   0   0   1   0 = 130
	//    0    1    1    1   1   1   1   1 = 127
	// 	  1    1    1    1   1   1   1   1 = 255

	var unsignedInteger8Bit uint8 = 255
	fmt.Printf("unsignedInteger8Bit hat den Wert %v und den Typ %v\n", unsignedInteger8Bit, unsignedInteger8Bit)

	//  ----------- float -----------

	// floats, floating point numbers, repräsentieren Zahlen mit nachkommastellen.
	// Go hat zwei float typen, float32 und float64
	// Wie ihr euch wahrscheinlich denken könnt, kann float64 größere Werte speichern.
	// Die Grundliegende Spezifikation von floats ist deutlich komplexer als die der integer,
	// und ich werde hier fürs erste nicht näher darauf eingehen ;)

	var floatA float64 = 10.012
	fmt.Printf("floatA hat den Wert %v und den Typ %T", floatA, floatA)

	//  --- string, rune und byte ---

	// string, Zeichenkette, ist ein komplexer typ, der primär zur Repräsentation von Text genutzt wird.
	// Komplex ist string deshalb, weil ein string eine Kette von Werten vom Typ byte ist.
	// Ein String ist eine endliche Folge von Zeichen (also z.B. Buchstaben, Ziffern etc.), aus einem
	// bestimmten Zeichensatz/Kodierung
	// In Go sind alle strings per UTF-8 enkodiert. Darüber später mehr.
	// Ein string hat die folgende Struktur:
	//
	//	type _string struct {
	//		elements *byte // Zugrundeliegende bytes
	//		len      int   // Anzahl der bytes
	//	}
	//
	// Um dies zu veranschaulichen, einige Beispiele:

	var s string        // Deklariert ohne Wert, nimmt also den Standardwert für String an: "". Eine Kette mit Länge 0
	s = "Hello, world!" // Nun da wir s einen Wert zugewiesen haben ist s nicht mehr ""
	fmt.Println(s)

	// Wie du siehst, haben wir den Wert von s in "" doppelte Anführungszeichen gesetzt. Dies signalisiert,
	// dass es sich um ein string handelt. Um innerhalb eines string " zu verwenden, müssen wir ene
	// davor einen Backslash "\" setzen. Möchten wir einen Backslash in einem string speichern setzen wir
	// davor wiederum einen backslash:
	s2 := "Ein Wert vom 'Typ' \"string\", und ein backslash \\"
	fmt.Println(s2)

	// Der Wert eines String kann außerdem noch in der "raw"-Form deklariert werden:
	var rawString string = `
	Ein String in raw-Form.
	Kann auch \ oder "" enthalten, ohne dass diese "esquaped" werden müssen.
	`
	fmt.Println(rawString)

	// Über Byte und Rune und die dem zugrundeliegenden Konzepte sprechen wir später :P
}
