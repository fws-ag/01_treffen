package main

import "fmt"

func main() {

	// Go's einziges Schleifenkonstrukt ist die for-Schleife, fortan als for-loop bezeichnet.
	// Eine for-Schleife (oder einfach nur for loop) ist eine Kontrollflussanweisung zur
	// Spezifizierung von Iterationen, die es ermöglicht, Code wiederholt auszuführen.

	//  ---------- Beispiel ---------

	// Nimm an, wir möchten die Zahlen 1 bis 10 in unser Terminal ausgeben. Weil das toll ist.
	// Wir könnten folgendes tun:

	fmt.Println("1")
	fmt.Println("2")
	fmt.Println("3")
	fmt.Println("4")
	fmt.Println("5")
	fmt.Println("6")
	fmt.Println("7")
	fmt.Println("8")
	fmt.Println("9")
	fmt.Println("10\n---")

	// Funktioniert, ist aber wirklich SEHR hässlich...

	// Stattdessen könnten wir ein Schleifenkonstrukt gebrauchen, und zwar so:

	countTo := 10

	for i := 1; i <= countTo; i++ {
		fmt.Println(i)
	}

	//  --------- Erklärung ---------
	// Okay, es gibt mehrere Sachen, die wir hier entschlüsseln müssen!
	// Fangen wir mit der Struktur des for loops an:
	//
	// Ein for loop besteht aus drei Komponenten, die durch ; getrennt sind.
	// Wir brauchen nicht immer alle drei, aber dazu später mehr.
	//
	// 1. init statement, hier "i := 1".
	// Das init statement wird einmalig vor der ersten Wiederholung der Schleife ausgeführt.
	//
	// 2. condition expression (Konditionsausdruck), hier "i < countTo".
	// Der Konditionsausdruck löst sich entweder in den Wert true oder false auf, und wird vor jeder
	// Wiederholung überprüft.
	//
	// Löst er in den Wert false auf, so stoppt die Schleife.
	//
	// 3. postcondition, hier i++ (i = i + 1).
	// Die Postanweisung wird am Ende jeder Wiederholung ausgeführt.
	//
	// Im obigen loop bewirkt sie, dass der Wert von i nach jeder Wiederholung um eins inkrementiert wird.
}
