// Auf eine Frage soll ZUFÄLLIG mit einer von 8 Möglichen Erwiderungen geantwortet werden.
// Die Erwiderungen sollten völlig generisch sein, sodass sie auf jede Frage passen.
//
// 1. "Mit Sicherheit!"
// 2. "Lieber nicht..."
// 3. "Das kann ich dir noch nicht verraten."
// 4. "Höchstwahrscheinlich"
// 5. "Die Zeichen deuten auf ein Ja hin."
// 6. "Sehr zweifelhaft."
// 7. "Ohne Zweifel."
// 8. "Konzentriere dich und frage noch einmal."

package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Hilfestellung

// genRandInt produziert einen int >= min, < max
func genRandInt(min, max int) int {
	return min + rand.Intn(max-min)
}

func main() {
	rand.Seed(time.Now().UnixNano()) // Hilfestellung

	randomNumber := genRandInt(1, 9)
	fmt.Print(randomNumber) // Nur zum veranschaulichen, kann entfernt werden

	//...
}
