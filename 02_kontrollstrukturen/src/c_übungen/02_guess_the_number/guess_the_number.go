// Schreibe ein Programm, in dem eine zufällige Nummer zwischen
// 1 und 100 erzeugt wird, die der Benutzer erraten muss.
//
// Wenn der Benutzer daneben liegt, so erhält er einen Tipp, ob seine Schätzung zu groß oder zu klein war.
// Liegt der Benutzer richtig, so wird ihm dies sowie die Anzahl der Versuche mitgeteilt die er brauchte,
// um die Nummer zu erraten und das Programm wird beendet.
//
// Ein Tipp, was den Fluss des Programms anbelangt:
//
// Precondition:
// - Es gibt eine Variable vom Typ integer mit einem Wert zwischen 1 und 100
//
// Algorithmus:
// - Ist der Input des Benutzer kleiner als die zufällig generierte Zahl?
//  -> Dem Benutzer wird mitgeteillt, dass seine Schätzung zu klein war.
//     Er darf nochmal raten.
//
// - Ist der Input des Benutzers größer als die zufällig generierte Zahl?
//  -> Dem Benutzer wird mitgeteillt, dass seine Schätzung zu groß war.
//     Er darf nochmal raten.
//
// - Ist der Input des Benutzers identisch zur zufällig generierten Zahl?
//  -> Dem Benutzer wird mitgeteilt, dass seine Schätzung korrekt war.
//     Dem Benutzer wird die Anzahl seiner Versuche mitgeteilt.
//	   Das Programm wird beendet.
//
// - Ist die Anzahl der Versuche größer als 10?
//  -> Das Programm wird beendet
//
// Postcondition:
// - Die Anzahl der Versuche ist nach jeder Schätzung eins mehr
//
// - Das Programm wurde beendet.
//   Entweder weil der Benutzer die zufällig generierte Zahl erraten hat,
//   weil die Anzahl der Versuche größer als 10 war,
//   oder weil er das Programm beendet hat.

package main

import (
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano()) // Hilfestellung

	// ...
}
