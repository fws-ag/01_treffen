//  -----------------------------  KONTROLLSTRUKTUREN  -----------------------------

// Kontrollstrukturen, Control Structures im Englischen, ermöglichen es, den Ablauf unseres Programms
// zu kontrollieren.
// Diese Tätigkeit, allgemeinhin als Flow Control bezeichnet, besteht darin, während
// der Ausführung des Programms Entscheidungen zu treffen, von denen der Fluss des Programms gelenkt wird bzw.
// wie das Programm auf bestimmte Konditionen und Parameter reagiert.
//
// Einige Begriffe sollte man sich im Kontext der Flow Control einprägen:
//
// Prä-Kondition/Preconditon:
// Der Zustand von Variablen bevor diese durch eine Kontrollstrukture beeinflusst werden.
//
// Algorithmus:
// Eine Abfolge von Schritten, die unter Berücksichtigung der Prä-Konditionen ausgeführt werden.
// Dies stellt die "Logik" der Kontrollstruktur dar.
//
// Post-Kondition/Postcondition:#
// Der Zustand von Variablen nachdem diese durch eine Kontrollstruktur beeinflusst wurden
// -> Das Ergebnis des Algorithmus.
//
// ---
// Ein Beispiel:
// Am Beispiel des Verkehrs können wir uns ein mentales Modell einer Kontrollstruktur verbildlichen:
//
// Ein Fahrzeug nähert sich einer Kreuzung, daraus folgt die Precondition: Das Fahrzeug ist in Bewegung.
// Nehmen wir an, dass die Ampel rot zeigt. Die Kontrollstruktur soll nun eine dem Kontext angemessene
// Vorgehensweise für das Fahrzeug bestimmen.
//
// Ansatz:
// Precondition: Das Fahrzeug ist in Bewegung | Die Ampel ist rot.
// Algorithmus:
// - Ist die Ampel grün? Wenn ja, so darf das Fahrzeug sich ungehindert weiter bewegen.
// - Ist die Ampel rot? Wenn ja, so muss das Fahrzeug stoppen.
// Postcondition: Das Fahrzeug kam zum Stehen.
// ---
//
// In Go gibt es drei primäre Kontrollstrukturen:
//
// - If-Then und If-Then-Else Statement
// - Switch Statement
// - For-Loop
//
// Alle dienen der Flow Control, sind aber in bestimmten Situationen angemessener als die Alternativen.
//
// Für das Thema Kontrollstrukturen ist der bool Typ besonders wichtig, wie Du gleich sehen wirst.
// Der bool Typ ist nämlich das Ergebnis aller Evaluierungen von Kondition, aber dazu unten mehr:

package main

import (
	"fmt"
	"time"
)

func main() {
	//  ----------------------------  VERGLEICHSOPERATOREN  ---------------------------

	// Bevor wir uns mit den Kontrollstrukturen befassen müsst ihr noch ein wichtiges Konzept
	// im Kopf haben: Vergleichsoperatoren (und logische Operatoren :p).
	//
	// Vergleichsoperatoren sind binäre Operatoren, d.h. sie können eins von zwei möglichen Ergebnissen
	// produzieren. Im falle von Go's Vergleichsoperatoren sind dies die Werte true und false vom Typ bool.
	//
	// Die Operatoren werden in einem Vergleich zweier Werte angewandt, und produzieren bei einem positiven
	// Vergleich den Wert true, bei einem negativen den Wert false.
	// Dieses Produkt nennt man Wahrheitswert.
	//
	// Die Struktur eines solchen Vergleichs sieht so aus:
	//
	// WERT1 OPERATOR WERT2
	//
	// Go stellt die folgenden Vergleichsoperatoren zur Verfügung:
	//
	// Operator   Bedeutung
	// --------   ---------
	// == -  	  ist identisch
	// != -       ist NICHT identisch
	// <  -       ist kleiner
	// <= -       ist kleiner oder identisch
	// >  -       ist größer
	// >= -       ist größer oder identisch
	//
	//
	// Wir können in Vergleichen auch mit den arithmetischen Operatoren Go's arbeiten:
	//
	// Operator  Name                  Typen
	// --------   ----                 -----
	// +          Plus                 integers, floats, complex values, strings
	// -          Minus                integers, floats, complex values
	// *          Mal              	   integers, floats, complex values
	// /          Geteilt/Quotient     integers, floats, complex values

	// %          Rest/Modulo          integers
	// &          bitwise AND          integers
	// |          bitwise OR           integers
	// ^          bitwise XOR          integers
	// &^         bit clear (AND NOT)  integers

	// <<         left shift           integer << unsigned integer
	// >>         right shift          integer >> unsigned integer
	//
	// Generell können nur Werte des selben Typs miteinander verglichen werden!

	//  ---------- Beispiel ---------

	// Vergleich zweier Werte, Produkt des Vergleiches wird per fmt.Println ausgegeben
	Wert1, Wert2 := 1, 10

	// Ergibt false, weil 1 nicht größer als 10 ist
	var ergebnisDerEvaluation bool = Wert1 > Wert2
	fmt.Printf("Das Ergebnis der Evaluation ist: %v\n", ergebnisDerEvaluation)

	// Wir können einen Vergleich auch direkt anwenden, ohne das Ergebnis in einer Variablen zu speichern:
	fmt.Printf("Evaluation Nr. 2: %v\n", Wert1 < Wert2)
	//  ------------- # -------------

	// Wir können in einem Vergleich auch arithmetische Operatoren verwenden:
	fmt.Printf("Wert2 mit 2 dividiert, Rest: %v\n=> Wert2 ist eine gerade Zahl: %v\n", Wert2%2, Wert2%2 == 0)

	//  ----------------------------  LOGISCHE OPERATOREN  ----------------------------
	// Logische Operatoren, auch Verknüpfungen genannt, komplementieren Vergleichsoperatoren in der Evaluation von Konditionen.
	// Sie ermöglichen es, einen Vergleich mit mehreren Konditionen durchzuführen.
	//
	// Operator   Name              Beschreibung
	// --------   ----              -----------
	// &&         conditional AND   p && q  bedeutet "wenn p dann q sonst falsch"
	// ||         conditional OR    p || q  bedeutet "wenn p, dann wahr, sonst q".
	// !          NOT               !p      bedeutet "nicht p"
	//
	// Anstatt die Werte 1 und 2 nur auf ihre Gleichheit hin zu untersuchen könnte man z.B. schauen ob der
	// Wert 1 kleiner ist als Wert 2
	// UND eine gerade Zahl:
	// Wert1 < Wert2 && Wert1 % 2 == 0
	//  ------------- # -------------

	//  ---------------------------------  IF - THEN  ---------------------------------
	// Die wohl simpleste Kontrollstruktur ist das If-Then Statement.
	// Es besteht aus einem Test, bei dem eine Kondition evaluiert wird, und einem Algorithmus, der
	// bei positiver Evaluation ausgeführt wird.
	//  ---------- Beispiel ----------

	// Precondition
	var iIfThen = 1

	// Struktur: if KONDITION {
	// ALGORITHMUS
	//	}

	// Kondition
	if iIfThen == 1 {
		// Algorithmus
		fmt.Printf("iIfThen war Anfangs %v\n", iIfThen)
		iIfThen := iIfThen + 1
		fmt.Printf("iIfThen ist jetzt eins mehr: %v\n", iIfThen)
	}
	// Wie Du siehst, ist die Struktur des If-Then Statements recht simpel:
	// WENN etwas so ist, wie ich es voraussetze DANN tu' was
	// Ist die Kondition nicht erfüllt, passiert nichts. Währe ifThen nicht 1, würde es also keinen Output geben.
	//  ------------- # -------------

	//  --------------------------------  IF-THEN-ELSE  -------------------------------

	// Das If-Then-Else Statement erweitert das If-Then Statement um eine Komponente: Die Else-Klause.
	//
	// Struktur:
	// WENN etwas so ist, wie ich es voraussetze DANN tu' ETWAS, ANSONSTEN tu' was ANDERES

	//  ---------- Beispiel ---------

	// Precondition
	var iIfThenElse = 100

	if iIfThenElse < 10 {
		// Eingebettetes if statement
		if iIfThenElse%2 == 0 {
			fmt.Println("iIfThenElse ist kleiner als 10 und gerade")
		} else {
			fmt.Println("iIfThenElse ist kleiner als 10 und ungerade")
		}
		// ---
	} else {
		// Deklaration innerhalb des else statements
		i := float64(iIfThenElse) / 5
		fmt.Printf("ifThenElse ist gleich oder größer als 10, konkret %v. Das geteilt durch 5 ist %v\n", iIfThenElse, i)
	}

	// Weil die If-Klause nicht positiv evaluiert wird, d.h. das Ergebnis der Evaluation ist false,
	// wird der Algorithmus der Klause nicht ausgeführt.
	// Stattdessen wird der Algorithmus der Else-Klause ausgeführt.

	// Wir können auch der Expression des if statements eine Deklaration vorausgehen lassen.
	// Die deklarierte Variable ist nur im Scope des if statements zugänglich:

	if i := iIfThenElse / 2; i > 5 {
		fmt.Printf("i ist %v\n", i)
	}

	//  -------- ELSE-IF-THEN -------
	// Auf ein else kann ein weiteres if folgen - also eine zweite Kondition, die evaluiert wird, falls das Ergebnis
	// der vorhergegangenen Evaluation negativ war
	// So können beliebig viele Konditionen verschachtelt werden:

	if i := 10; i/2 == 55 {
		// Kondition ist negativ, dieser Block wird also übersprungen
	} else if i == 50 {
		// Immernoch negativ, wieder übersprungen
	} else if i == 5 {
		fmt.Println("Yep")
	} else {
		// bis hier Fließt das Programm nicht ;)
	}

	// Ihr könnt euch sicher vorstellen, dass dieses Konstrukt schnell unübersichtlich werden kann, wenn es viele Konditionen
	// gibt, deren Wahrheitswert man ermitteln möchte.
	// Um eine übersichtlichere Art zu bieten, dies zu bewerkstelligen, gibt es eine weitere Kontrollstruktur;
	// Das Switch Statement:
	//  ------------- # -------------

	//  ------------------------------  SWITCH-STATEMENT  -----------------------------

	// Das Switch Statement bietet eine übersichtliche Struktur, um eine Vielzahl möglicher Fälle zu evaluieren.
	// Die Regeln eines Vergleichs sind die selben wie beim If-Statement.
	//
	// Die generelle Struktur eines Switch Statements sieht so aus:
	//
	// PRECONDITION
	// FALL1 VERGLEICH ALGORITHMUS
	// FALL2 VERGLEICH ALGORITHMUS
	// FALLn VERGLEICH ALGORITHMUS
	// DEFAULT ALGORITHMUS (Optional)
	//
	// Ein Switch Statement muss nicht alle möglichen Fälle abdecken, kann dies aber indem wir einen Standardalgorithmus
	// bereitstellen der dann ausgeführt wird, wenn keiner der festgelegten Fälle eintritt.

	//  ---------- Beispiel ---------
	var iSwitchStatement = 3

	// 	   Expression
	switch iSwitchStatement {
	//   Kondition: iSwitchStatement == 1
	case 1:
		// Algorithmus
		fmt.Println("Eins!")
	case 2:
		fmt.Println("Zwei!")
	case 3:
		fmt.Println("Drei!")
	default:
		fmt.Println("Weiter kann ich nicht zählen...")

	}

	// Wir können uns innerhalb eines Switch Statements auch explizit auf außerhalb dessen
	// deklarierte Variablen beziehen, und die Expression weglassen:
	switch {
	case iSwitchStatement == 1:
		fmt.Println("Eins!")
	case iSwitchStatement == 2:
		fmt.Println("Zwei!")
	case iSwitchStatement == 3:
		fmt.Println("Auch Drei!")
		fallthrough
	case false:
		fmt.Println("Dieser Case wird wegen dem fallthroug ausgelöst, obwohl seine Expression nicht true ergibt")
	}

	// Hier benutzen wir die time Bibliothek, Teil von Go's Standard Library.
	// Wir müssen keine Variable deklarieren, weil time.Now.Weekday() eine Expression ist, die während der Ausführung
	// einen Wert produziert, wenn sie aufgerufen wird.
	switch time.Now().Weekday() {
	case time.Thursday:
		fmt.Println("Es ist Dienstag => AG, yay! :D")
	case time.Saturday:
		fmt.Println("Samstag ist auch okay :}")
	default:
		fmt.Println("Es ist irgendein anderer Tag als Dienstag => Keine AG :(")
	}

}
