package main

import (
	"fmt"
	"math/rand"
)

func main() {
	randomNumber := rand.Intn(8)

	fmt.Println("Frag' mich etwas:")
	fmt.Scanln()

	switch randomNumber {
	case 0:
		fmt.Println("Mit Sicherheit!")
	case 1:
		fmt.Println("Lieber nicht...")
	case 2:
		fmt.Println("Das kann ich dir noch nicht sagen.")
	case 3:
		fmt.Println("Höchstwahrscheinlich")
	case 4:
		fmt.Println("Die Zeichen deuten auf ein Ja hin.")
	case 5:
		fmt.Println("Sehr zweifelhaft.")
	case 6:
		fmt.Println("Ohne Zweifel.")
	case 7:
		fmt.Println("Konzentriere dich und frage noch einmal.")
	}
}
