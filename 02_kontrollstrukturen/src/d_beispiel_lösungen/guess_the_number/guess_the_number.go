package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Setze einen Seed basierend auf der Anzahl von Nanosekunden, die seit
	// dem 01. Januar 1970 UTC vergangen sind
	rand.Seed(time.Now().UnixNano())

	// Intn() generiert einen zufälligen Int zwischen 0 und dem Parameter der Funktion.
	// Weil wir als Minimum 1 und nicht 0 haben möchten, geben wir der Funktion als Parameter
	// (max - min) + min | (100 - 1) + 1, es muss also in jedem Fall eine Zahl über 0 raus kommen.
	min := 1
	max := 100
	secretRandomNumber := rand.Intn(max-min) + min

	fmt.Println("Guess a number between 1 and 100:")

	var guess int // guess wird der Wert zugewiesen, denn du in das Terminal (genauer stdin) eingibst

	var attempts int // attemps wird bei jeder Wiederholung der Schleife um 1 inkrementiert

loop: // loop ist ein Label
	// Kondition: Während guess NICHT gleich ist wie secretRandomNumber läuft der Loop
	for {
		attempts++
		fmt.Print("Input your guess:\n=> ")
		fmt.Scanln(&guess)

		switch {
		case attempts > 10:
			fmt.Println("Du hast zu viele Versuche gebraucht. ENTTÄUSCHEND :(")
			break loop
		case guess < secretRandomNumber:
			fmt.Printf("Too small! This was your %v. attempt\n", attempts)
		case guess > secretRandomNumber:
			fmt.Printf("Too big! This was your %v. attempt\n", attempts)
		case guess == secretRandomNumber:
			fmt.Printf("You win! The secret number was %v\nIt took you %v attempts.",
				secretRandomNumber, attempts)
			break loop
			// Würden wir hier das break statement nicht auf das Label loop beziehen,
			// so würden wir lediglich das switch statement beenden
		}
	}
}
